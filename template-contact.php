<?php
/**
 * Template Name: Contact Template
 */
?>
<?php get_header(); global $post; $is_en=false; if(get_locale() == 'en_US'){$is_en = true;}?>
<div class="department-header">
	<div class="container">
		<div class="row">
			<div class="col-5 col-sm-4 col-md-2">
				<h1><?= get_the_title($post); ?></h1>
			</div>
			<div class="col-7 col-sm-8 col-md-10">
				<hr>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid holder--contact">
	<div class="overlay"></div>
    <div class="row contact container-lg">
        <div class="col-lg-6 contact__form">
            <form class="form">
                <h1><?php the_field('contact_form'); ?></h1>
                <div class="tabs">
                    <a id="contact-fields-tab" class="tabs__tab active">
                        <?php the_field('contact_fields'); ?>
                    </a>
                    <a id="date-fields-tab" class="tabs__tab">
                        <?php the_field('contact_date_and_apartment'); ?>
                    </a>
                </div>
                <div class="tabs-contents">
                    <div id="contact-fields-tab-content" class="tabs-contents__tab active">
                        <input type="text" placeholder="<?php the_field('contact_form_name'); ?>" name="name" required>
                        <input type="email" placeholder="<?php the_field('contact_form_email'); ?>" name="email"
                               required>
                        <input type="text" placeholder="<?php the_field('contact_form_number'); ?>" name="phone-number"
                               required>

                        <div class="messages-first"></div>
                        <a id="next-tab" class="button inverted"><?php the_field('contact_form_continue'); ?><i
                                    class="fas fa-arrow-right"></i></a>
                    </div>
                    <div id="date-fields-tab-content" class="tabs-contents__tab">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-icon">
                                    <input type="text" placeholder="<?php the_field('contact_form_from_to'); ?>"
                                           name="from_to" required>
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <input type="number" placeholder="<?php the_field('contact_form_persons'); ?>" min="1"
                                       value="1" name="persons" required>
                            </div>
                        </div>
                        <select name="villa">
                            <?php
                            if (have_rows('contact_villas')):
                                while (have_rows('contact_villas')) : the_row(); ?>
                                    <option value="<?php the_sub_field('contact_villa_page'); ?>"><?php the_sub_field('contact_villa'); ?></option>
                                <?php
                                endwhile;
                            endif;
                            ?>
                        </select>
                        <textarea placeholder="Správa" name="message"></textarea>
                        <label><input type="checkbox" name="privacy-policy"
                                      value="check"> <?php the_field('contact_privacy_policy'); ?></label>
                        <div class="messages-last"></div>
                        <button class="button inverted" id="finish-form"><?php the_field('contact_form_submit'); ?><i
                                    class="fas fa-arrow-right"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-6 contact__right">
            <h1><?php the_field('contact_where_to_find_us'); ?></h1>
            <div class="contact__right__message">
                <a class="row no-gutters bounce-link" href="<?php the_field('contact_maps_url'); ?>">
                    <span class="col-lg-6 contact__right__message__text">
                        <?php the_field('contact_go_to_maps'); ?>
                        <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="col-lg-6">
                        <span id="map-contact" class="map"></span>
                        <script>
                            maps.push(function () {
                                var point = {
                                    lat: <?php the_field('contact_lat'); ?>,
                                    lng: <?php the_field('contact_lng'); ?>
                                };
                                var map = new google.maps.Map(document.getElementById('map-contact'), {
                                    zoom: 4,
                                    center: point
                                });
                                var marker = new google.maps.Marker({position: point, map: map});
                            });
                        </script>
                    </span>
                </a>
            </div>
            <div class="contact__right__message">
                <a class="row no-gutters bounce-link" href="<?php the_field('contact_resort_maps_url'); ?>"  data-lightbox="resort-map">
                    <span class="col-lg-6 contact__right__message__text">
                        <?php the_field('contact_show_resort_map'); ?>
                        <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="col-lg-6 contact__right__resort-image" style="background-image: url('<?php the_field('contact_resort_maps_url'); ?>'); background-size: cover;"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        /* select selected from referrer */
        var ref = new URL(document.referrer);
        $("select option").each(function (key, option) {
            var $opt = $(option);
            if (new URL($opt.val(), ref.origin).pathname === ref.pathname){
                $opt.prop('selected', true)
            }
        });
        $('input[name="from_to"]').daterangepicker({
            locale: {
                applyLabel: "OK"
            }
        });
        $("#next-tab, #date-fields-tab").click(function () {
            var messages = $('.messages-first');
            messages.empty();
            var good = true
            if (!$('input[name="name"]').get(0).checkValidity() || $('input[name="name"]').val().trim().length === 0) {
                messages.append('<div class="message message--fail">Meno nesmie byť prázdne.</div>');
                good = false;
            }
            if (!$('input[name="email"]').get(0).checkValidity()) {
                messages.append('<div class="message message--fail">Neplatný email.</div>');
                good = false;
            }
            if (!$('input[name="phone-number"]').get(0).checkValidity() || $('input[name="phone-number"]').val().match(/^(0|\+[0-9]{3})\.?[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}$/g) === null) {
                messages.append('<div class="message message--fail">Zlý formát telefónneho čísla.</div>');
                good = false;
            }
            if (good) {
                $("#contact-fields-tab").removeClass("active");
                $("#contact-fields-tab-content").removeClass("active");
                $("#date-fields-tab").addClass("active");
                $("#date-fields-tab").removeClass("disabled");
                $("#date-fields-tab-content").addClass("active");
            }
        });
        $("#contact-fields-tab").click(function () {
            if (!$(this).hasClass("active")) {
                $("#date-fields-tab").removeClass("active");
                $("#date-fields-tab-content").removeClass("active");
                $("#contact-fields-tab").addClass("active");
                $("#contact-fields-tab-content").addClass("active");
            }
        });
    });
</script>
<footer>
	<div class="container-lg">
		<div class="row">
			<div class="col-md-8 col-sm-12">
				<div class="row">
					<div class="col-sm-6 footer-logo">
						<?php
						$custom_logo_id = get_theme_mod('custom_logo');
						$logo = wp_get_attachment_image_src($custom_logo_id, 'full');
						if (has_custom_logo()) {
							echo '<img style="width: 190px;height: auto;" src="' . esc_url($logo[0]) . '" alt="' . get_bloginfo('name') . '">';
						} else {
							echo get_bloginfo('name');
						}
						?>
					</div>
					<div class="col-sm-6 info text-sm-left">
						<div class="row">
							<div class="col-12 col-sm-2">
								<i class="far fa-user"></i>
							</div>
							<div class="col-12 col-sm-10 info__text">
								<a href="tel:<?php the_field($is_en == true ? 'footer_phone_number_en' : 'footer_phone_number' , 'option');?>"><?php the_field('footer_phone_number', 'option'); ?></a>
								<a href="mailto:<?php the_field($is_en == true ? 'footer_email_en' : 'footer_email', 'option');?>"><?php the_field($is_en == true ? 'footer_email_en' : 'footer_email', 'option');?></a>
								<span><?php the_field($is_en == true ? 'footer_name_en' : 'footer_name', 'option'); ?></span>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-2">
								<i class="fas fa-map-marker-alt"></i>
							</div>
							<div class="col-12 col-sm-10 info__text">
								<span><?php the_field($is_en == true ? 'footer_street_en' : 'footer_street', 'option'); ?></span>
								<span><?php the_field($is_en == true ? 'footer_city_en' : 'footer_city', 'option'); ?></span>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 social">
								<a href="<?php the_field($is_en == true ? 'footer_facebook_en' : 'footer_facebook', 'option'); ?>">
									<i class="fab fa-facebook-square fa-2x"></i>
								</a>
								<a href="<?php the_field($is_en == true ? 'footer_instagram_en' : 'footer_instagram', 'option'); ?>">
									<i class="fab fa-instagram fa-2x"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-12">
				<div class="row  no-gutters">
					<div class="col-lg-12">
						<div id="map"></div>
						<script>
							maps.push(function () {
								var point = {
									lat: <?=get_field($is_en == true ? 'footer_lat_en' : 'footer_lat', 'option') ? get_field($is_en == true ? 'footer_lat_en' : 'footer_lat', 'option') : 0; ?>,
									lng: <?=get_field($is_en == true ? 'footer_lng_en' : 'footer_lng', 'option') ? get_field($is_en == true ? 'footer_lng_en' : 'footer_lng', 'option') : 0; ?>
								};
								var map = new google.maps.Map(document.getElementById('map'), {zoom: 4, center: point});
								var marker = new google.maps.Marker({position: point, map: map});
							});
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright"></div>
</footer>
<script>
    function initMaps() {
        for (let i in maps) {
            maps[i]();
        }
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=<?php the_field('google_maps_api_key', 'option'); ?>&callback=initMaps">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/dist/scripts/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>
