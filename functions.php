<?php
function custom_styles()
{
    wp_enqueue_style('theme-style', get_template_directory_uri() . '/dist/styles/app.css');
}

add_action('wp_enqueue_scripts', 'custom_styles');

function register_primary_menu()
{
    register_nav_menu('primary', __('Primary Menu', 'sajdiky'));
	register_nav_menu('language', __('Language menu', 'sajdiky'));
	add_theme_support('custom-logo');
}

add_action('after_setup_theme', 'register_primary_menu');

if (function_exists('acf_add_options_sub_page') && function_exists('acf_add_options_page') && function_exists('acf_add_local_field_group') && function_exists('acf_add_local_field')):
    /* OPTIONS PAGES */
    $general = acf_add_options_page(array(
        'page_title' => 'Nastavenie šablóny',
        'menu_title' => 'Nastavenie šablóny',
        'menu_slug' => 'template-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
    $general_en = acf_add_options_page(array(
        'page_title' => 'Template Settings',
        'menu_title' => 'Template Settings',
        'menu_slug' => 'template-settings-en',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
    $footer = acf_add_options_sub_page(array(
        'page_title' => 'Nastavenie pätky',
        'menu_title' => 'Nastavenie pätky',
        'menu_slug' => 'footer-settings',
        'capability' => 'edit_posts',
        'redirect' => false,
        'parent_slug' => $general['menu_slug'],
    ));
	$footer_en = acf_add_options_sub_page(array(
		'page_title' => 'Footer Settings',
		'menu_title' => 'Footer Settings',
		'menu_slug' => 'footer-en-settings',
		'capability' => 'edit_posts',
		'redirect' => false,
		'parent_slug' => $general_en['menu_slug'],
	));
    $villa = acf_add_options_sub_page(array(
        'page_title' => 'Nastavenie stránky s vilou',
        'menu_title' => 'Nastavenie stránky s vilou',
        'menu_slug' => 'villa-page-settings',
        'capability' => 'edit_posts',
        'redirect' => false,
        'parent_slug' => $general['menu_slug'],
    ));
    $villa_en = acf_add_options_sub_page(array(
        'page_title' => 'Villa Page Settings EN',
        'menu_title' => 'Villa Page Settings EN',
        'menu_slug' => 'villa-page-settings-en',
        'capability' => 'edit_posts',
        'redirect' => false,
        'parent_slug' => $general_en['menu_slug'],
    ));

    /* GENERAL */
    acf_add_local_field_group(array(
        'key' => 'general_group',
        'title' => 'General',
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => $general['menu_slug'],
                ),
            ),
        ),
    ));
    acf_add_local_field(array(
        'key' => 'field_read_more',
        'label' => 'Čítaj viac',
        'name' => 'read_more',
        'type' => 'text',
        'parent' => 'general_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_order_now_text',
        'label' => 'Objednať teraz',
        'name' => 'order_now_text',
        'type' => 'text',
        'parent' => 'general_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_order_text',
        'label' => 'Objednávka',
        'name' => 'order_text',
        'type' => 'text',
        'parent' => 'general_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_order_now_page',
        'label' => 'Stránka objednávky',
        'name' => 'order_page',
        'type' => 'page_link',
        'parent' => 'general_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_phone_number',
        'label' => 'Telefónne číslo',
        'name' => 'phone_number',
        'type' => 'text',
        'parent' => 'general_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_google_maps_api_key',
        'label' => 'Google Maps API Kľúč',
        'name' => 'google_maps_api_key',
        'type' => 'text',
        'parent' => 'general_group'
    ));

    /* GENERAL EN */
    acf_add_local_field_group(array(
        'key' => 'general_group_en',
        'title' => 'General',
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => $general_en['menu_slug'],
                ),
            ),
        ),
    ));
    acf_add_local_field(array(
        'key' => 'field_read_more_en',
        'label' => 'Read more text',
        'name' => 'read_more_en',
        'type' => 'text',
        'parent' => 'general_group_en'
    ));
    acf_add_local_field(array(
        'key' => 'field_order_now_text_en',
        'label' => 'Order now text',
        'name' => 'order_now_text_en',
        'type' => 'text',
        'parent' => 'general_group_en'
    ));
    acf_add_local_field(array(
        'key' => 'field_order_text_en',
        'label' => 'Order text',
        'name' => 'order_text_en',
        'type' => 'text',
        'parent' => 'general_group_en'
    ));
    acf_add_local_field(array(
        'key' => 'field_order_now_page_en',
        'label' => 'Order page',
        'name' => 'order_page_en',
        'type' => 'page_link',
        'parent' => 'general_group_en'
    ));
    acf_add_local_field(array(
        'key' => 'field_phone_number_en',
        'label' => 'Phone number text',
        'name' => 'phone_number_en',
        'type' => 'text',
        'parent' => 'general_group_en'
    ));
    acf_add_local_field(array(
        'key' => 'field_google_maps_api_key_en',
        'label' => 'Google Maps API Key',
        'name' => 'google_maps_api_key_en',
        'type' => 'text',
        'parent' => 'general_group_en'
    ));

    /* VILLA PAGE */
    acf_add_local_field_group(array(
        'key' => 'villa_page_group',
        'title' => 'Villa page',
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => $villa['menu_slug'],
                ),
            ),
        ),
    ));
    acf_add_local_field(array(
        'key' => 'field_locality',
        'label' => 'Lokalita',
        'name' => 'locality',
        'type' => 'text',
        'parent' => 'villa_page_group',
    ));
    acf_add_local_field(array(
        'key' => 'field_request_offer',
        'label' => 'Vyžiadať ponuku',
        'name' => 'request_offer',
        'type' => 'text',
        'parent' => 'villa_page_group',
    ));
    acf_add_local_field(array(
        'key' => 'field_download_home_rules_text',
        'label' => 'Stiahnuť domový poriadok',
        'name' => 'download_home_rules_text',
        'type' => 'text',
        'parent' => 'villa_page_group',
    ));
    acf_add_local_field(array(
        'key' => 'field_important_info_title',
        'label' => 'Dôležité informácie',
        'name' => 'important_info_title',
        'type' => 'text',
        'parent' => 'villa_page_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_pricelist_title',
        'label' => 'Cenník',
        'name' => 'pricelist_title',
        'type' => 'text',
        'parent' => 'villa_page_group'
    ));

    /* VILLA PAGE EN*/
    acf_add_local_field_group(array(
        'key' => 'villa_page_group_en',
        'title' => 'Villa page',
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => $villa_en['menu_slug'],
                ),
            ),
        ),
    ));
    acf_add_local_field(array(
        'key' => 'field_locality_en',
        'label' => 'Locality text',
        'name' => 'locality_en',
        'type' => 'text',
        'parent' => 'villa_page_group_en',
    ));
    acf_add_local_field(array(
        'key' => 'field_request_offer_en',
        'label' => 'Request offer text',
        'name' => 'request_offer_en',
        'type' => 'text',
        'parent' => 'villa_page_group_en',
    ));
    acf_add_local_field(array(
        'key' => 'field_download_home_rules_text_en',
        'label' => 'Download home rules text',
        'name' => 'download_home_rules_text_en',
        'type' => 'text',
        'parent' => 'villa_page_group_en',
    ));
    acf_add_local_field(array(
        'key' => 'field_important_info_title_en',
        'label' => 'Important info title',
        'name' => 'important_info_title_en',
        'type' => 'text',
        'parent' => 'villa_page_group_en'
    ));
    acf_add_local_field(array(
        'key' => 'field_pricelist_title_en',
        'label' => 'Price list title',
        'name' => 'pricelist_title_en',
        'type' => 'text',
        'parent' => 'villa_page_group_en'
    ));

    /* HOMEPAGE */
    acf_add_local_field_group(array(
        'key' => 'homepage_group',
        'title' => 'Homepage',
        'location' => array(
            array(
                array(
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => "template-home.php",
                ),
            ),
        ),
    ));
    /* HOMEPAGE --- villas */
    for ($i = 1; $i <= 3; $i++) {
        acf_add_local_field(array(
            'key' => 'field_villa_' . $i . '_title',
            'label' => 'Villa ' . $i . ' Title',
            'name' => 'villa_' . $i . '_title',
            'type' => 'text',
            'parent' => 'homepage_group'
        ));
        acf_add_local_field(array(
            'key' => 'field_villa_' . $i . '_description',
            'label' => 'Villa ' . $i . ' Description',
            'name' => 'villa_' . $i . '_description',
            'type' => 'textarea',
            'parent' => 'homepage_group'
        ));
        acf_add_local_field(array(
            'key' => 'field_villa_' . $i . '_image',
            'label' => 'Villa ' . $i . ' Image',
            'name' => 'villa_' . $i . '_image',
            'type' => 'image',
            'parent' => 'homepage_group',
            'return_format' => 'url',
        ));
        acf_add_local_field(array(
            'key' => 'field_villa_' . $i . '_page',
            'label' => 'Villa ' . $i . ' Page',
            'name' => 'villa_' . $i . '_page',
            'type' => 'page_link',
            'parent' => 'homepage_group',
        ));
    }
    /* HOMEPAGE --- rest */
    acf_add_local_field(array(
        'key' => 'field_guaranteed_rest_title',
        'label' => 'Guaranteed Rest Title',
        'name' => 'guaranteed_rest_title',
        'type' => 'text',
        'parent' => 'homepage_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_guaranteed_rest_text',
        'label' => 'Guaranteed Rest Text',
        'name' => 'guaranteed_rest_text',
        'type' => 'wysiwyg',
        'parent' => 'homepage_group'
    ));
    /* HOMEPAGE --- next project */
    acf_add_local_field(array(
        'key' => 'field_next_project_text',
        'label' => 'Next Project Text',
        'name' => 'next_project_text',
        'type' => 'text',
        'parent' => 'homepage_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_golf_resort_text',
        'label' => 'Golf resort text',
        'name' => 'golf_resort_text',
        'type' => 'text',
        'parent' => 'homepage_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_golf_resort_image',
        'label' => 'Golf resort image',
        'name' => 'golf_resort_image',
        'type' => 'image',
        'parent' => 'homepage_group',
        'return_format' => 'url',
    ));
    acf_add_local_field(array(
        'key' => 'field_golf_resort_page',
        'label' => 'Golf resort page',
        'name' => 'golf_resort_page',
        'type' => 'link',
        'parent' => 'homepage_group',
	    'return_format' => 'url'
    ));

    /* FOOTER */
    acf_add_local_field_group(array(
        'key' => 'footer_group',
        'title' => 'Footer',
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => $footer['menu_slug'],
                ),
            ),
        ),
    ));
    acf_add_local_field(array(
        'key' => 'field_footer_phone_number',
        'label' => 'Telefónne číslo',
        'name' => 'footer_phone_number',
        'type' => 'text',
        'parent' => 'footer_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_footer_email',
        'label' => 'Email',
        'name' => 'footer_email',
        'type' => 'email',
        'parent' => 'footer_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_footer_name',
        'label' => 'Meno',
        'name' => 'footer_name',
        'type' => 'text',
        'parent' => 'footer_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_footer_street',
        'label' => 'Ulica',
        'name' => 'footer_street',
        'type' => 'text',
        'parent' => 'footer_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_footer_city',
        'label' => 'Mesto',
        'name' => 'footer_city',
        'type' => 'text',
        'parent' => 'footer_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_footer_lat',
        'label' => 'Mapa latitude',
        'name' => 'footer_lat',
        'type' => 'number',
        'parent' => 'footer_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_footer_lng',
        'label' => 'Mapa longitude',
        'name' => 'footer_lng',
        'type' => 'number',
        'parent' => 'footer_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_footer_facebook',
        'label' => 'Facebook stránka URL',
        'name' => 'footer_facebook',
        'type' => 'text',
        'parent' => 'footer_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_footer_instagram',
        'label' => 'Instagram stránka URL',
        'name' => 'footer_instagram',
        'type' => 'text',
        'parent' => 'footer_group'
    ));
    // end //

	/* FOOTER EN */
	acf_add_local_field_group(array(
		'key' => 'footer_group_en',
		'title' => 'Footer EN',
		'location' => array(
			array(
				array(
					'param' => 'options_page',
					'operator' => '==',
					'value' => $footer_en['menu_slug'],
				),
			),
		),
	));
	acf_add_local_field(array(
		'key' => 'field_footer_phone_number_en',
		'label' => 'Phone number',
		'name' => 'footer_phone_number_en',
		'type' => 'text',
		'parent' => 'footer_group_en'
	));
	acf_add_local_field(array(
		'key' => 'field_footer_email_en',
		'label' => 'Email',
		'name' => 'footer_email_en',
		'type' => 'email',
		'parent' => 'footer_group_en'
	));
	acf_add_local_field(array(
		'key' => 'field_footer_name_en',
		'label' => 'Name',
		'name' => 'footer_name_en',
		'type' => 'text',
		'parent' => 'footer_group_en'
	));
	acf_add_local_field(array(
		'key' => 'field_footer_street_en',
		'label' => 'Street',
		'name' => 'footer_street_en',
		'type' => 'text',
		'parent' => 'footer_group_en'
	));
	acf_add_local_field(array(
		'key' => 'field_footer_city_en',
		'label' => 'City',
		'name' => 'footer_city_en',
		'type' => 'text',
		'parent' => 'footer_group_en'
	));
	acf_add_local_field(array(
		'key' => 'field_footer_lat_en',
		'label' => 'Map latitude',
		'name' => 'footer_lat_en',
		'type' => 'number',
		'parent' => 'footer_group_en'
	));
	acf_add_local_field(array(
		'key' => 'field_footer_lng_en',
		'label' => 'Map longitude',
		'name' => 'footer_lng_en',
		'type' => 'number',
		'parent' => 'footer_group_en'
	));
	acf_add_local_field(array(
		'key' => 'field_footer_facebook_en',
		'label' => 'Facebook page URL',
		'name' => 'footer_facebook_en',
		'type' => 'text',
		'parent' => 'footer_group_en'
	));
	acf_add_local_field(array(
		'key' => 'field_footer_instagram_en',
		'label' => 'Instagram page URL',
		'name' => 'footer_instagram_en',
		'type' => 'text',
		'parent' => 'footer_group_en'
	));

    /* VILLA TEMPLATE */
    acf_add_local_field_group(array(
        'key' => 'villa_group',
        'title' => 'Villa info',
        'location' => array(
            array(
                array(
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => "template-villa.php",
                ),
            ),
        ),
    ));
    acf_add_local_field(array(
        'key' => 'field_villa_title',
        'label' => 'Villa name',
        'name' => 'villa_title',
        'type' => 'text',
        'parent' => 'villa_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_villa_gallery',
        'label' => 'Villa gallery',
        'name' => 'villa_gallery',
        'type' => 'gallery',
        'return_format' => 'array',
        'parent' => 'villa_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_villa_description',
        'label' => 'Villa description',
        'name' => 'villa_description',
        'type' => 'wysiwyg',
        'parent' => 'villa_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_villa_equipment_title',
        'label' => 'Villa equipment title',
        'name' => 'villa_equipment_title',
        'type' => 'text',
        'parent' => 'villa_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_villa_equipment',
        'label' => 'Villa equipment',
        'name' => 'villa_equipment_repeater',
        'type' => 'repeater',
        'layout' => 'table',
        'parent' => 'villa_group',
        'sub_fields' => array(
            array(
                'key' => 'field_equipment',
                'label' => 'List',
                'name' => 'villa_equipment_list',
                'type' => 'wysiwyg',
            ),
        ),
    ));
    acf_add_local_field(array(
        'key' => 'field_download_home_rules_file',
        'label' => 'Home rules file',
        'name' => 'download_home_rules_file',
        'type' => 'file',
        'parent' => 'villa_group',
        'return_format' => 'url',
    ));
    acf_add_local_field(array(
        'key' => 'field_important_info_text',
        'label' => 'Important info text',
        'name' => 'important_info_text',
        'type' => 'wysiwyg',
        'parent' => 'villa_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_pricelist_text',
        'label' => 'Price list text',
        'name' => 'pricelist_text',
        'type' => 'wysiwyg',
        'parent' => 'villa_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_pricelist_page',
        'label' => 'Price list page',
        'name' => 'pricelist_page',
        'type' => 'page_link',
        'parent' => 'villa_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_next_villa_title',
        'label' => 'Next villa title',
        'name' => 'next_villa_title',
        'type' => 'text',
        'parent' => 'villa_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_next_villa_page',
        'label' => 'Next villa page',
        'name' => 'next_villa_page',
        'type' => 'page_link',
        'parent' => 'villa_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_prev_villa_title',
        'label' => 'Prev villa title',
        'name' => 'prev_villa_title',
        'type' => 'text',
        'parent' => 'villa_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_prev_villa_page',
        'label' => 'Prev villa page',
        'name' => 'prev_villa_page',
        'type' => 'page_link',
        'parent' => 'villa_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_villa_lat',
        'label' => 'Map latitude',
        'name' => 'villa_lat',
        'type' => 'number',
        'parent' => 'villa_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_villa_lng',
        'label' => 'Map longitude',
        'name' => 'villa_lng',
        'type' => 'number',
        'parent' => 'villa_group'
    ));

    /* RESORT TEMPLATE */
    acf_add_local_field_group(array(
        'key' => 'department_group',
        'title' => 'Department info',
        'location' => array(
            array(
                array(
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => "template-department.php",
                ),
            ),
        ),
    ));
    acf_add_local_field(array(
        'key' => 'field_resort',
        'label' => 'Resort title',
        'name' => 'resort',
        'type' => 'text',
        'parent' => 'department_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_departments_repeater',
        'label' => 'Departments',
        'name' => 'departments_repeater',
        'type' => 'repeater',
        'parent' => 'department_group',
        'layout' => 'block',
        'sub_fields' => array(
            array(
                'key' => 'field_departments_department_name',
                'label' => 'Name',
                'name' => 'department_name',
                'type' => 'text',
            ),
            array(
                'key' => 'field_departments_department_text',
                'label' => 'Info text',
                'name' => 'department_text',
                'type' => 'text',
            ),
            array(
                'key' => 'field_departments_department_email',
                'label' => 'Email',
                'name' => 'department_email',
                'type' => 'email',
            ),
            array(
                'key' => 'field_departments_department_phone_number',
                'label' => 'Phone number',
                'name' => 'department_phone_number',
                'type' => 'text',
            ),
            array(
                'key' => 'field_departments_department_time',
                'label' => 'Open time',
                'name' => 'department_time',
                'type' => 'text',
            ),
	        array(
		        'key' => 'field_departments_department_website',
		        'label' => 'Website',
		        'name' => 'department_website',
		        'type' => 'link',
		        'return_format' => 'array',
	        ),
        )
    ));
    acf_add_local_field(array(
        'key' => 'field_lunch_menu_text',
        'label' => 'Lunch menu text',
        'name' => 'lunch_menu_text',
        'type' => 'text',
        'parent' => 'department_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_lunch_menu_file',
        'label' => 'Lunch menu file',
        'name' => 'lunch_menu_file',
        'type' => 'file',
        'parent' => 'department_group',
        'return_format' => 'url',
    ));
    acf_add_local_field(array(
        'key' => 'field_drink_menu_text',
        'label' => 'Drink menu text',
        'name' => 'drink_menu_text',
        'type' => 'text',
        'parent' => 'department_group'
    ));
    acf_add_local_field(array(
        'key' => 'field_drink_menu_file',
        'label' => 'Drink menu file',
        'name' => 'drink_menu_file',
        'type' => 'file',
        'parent' => 'department_group',
        'return_format' => 'url',
    ));

    /* CONTACT TEMPLATE */
    acf_add_local_field_group(array(
        'key' => 'contact_group',
        'title' => 'Contact info',
        'location' => array(
            array(
                array(
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => "template-contact.php",
                ),
            ),
        ),
    ));
    acf_add_local_field(array(
        'key' => 'field_contact_go_to_maps',
        'label' => 'Go to maps',
        'name' => 'contact_go_to_maps',
        'type' => 'text',
        'parent' => 'contact_group',
    ));

    acf_add_local_field(array(
        'key' => 'field_contact_lat',
        'label' => 'Go to maps latitude',
        'name' => 'contact_lat',
        'type' => 'number',
        'parent' => 'contact_group',
    ));
    acf_add_local_field(array(
        'key' => 'field_contact_lng',
        'label' => 'Go to maps longitude',
        'name' => 'contact_lng',
        'type' => 'number',
        'parent' => 'contact_group',
    ));
    acf_add_local_field(array(
        'key' => 'field_contact_maps_url',
        'label' => 'Map url',
        'name' => 'contact_maps_url',
        'type' => 'text',
        'parent' => 'contact_group',
    ));

    acf_add_local_field(array(
        'key' => 'field_contact_resort_lat',
        'label' => 'Go to maps resort latitude',
        'name' => 'contact_resort_lat',
        'type' => 'number',
        'parent' => 'contact_group',
    ));
    acf_add_local_field(array(
        'key' => 'field_contact_resort_lng',
        'label' => 'Go to maps resort longitude',
        'name' => 'contact_resort_lng',
        'type' => 'number',
        'parent' => 'contact_group',
    ));
    acf_add_local_field(array(
        'key' => 'field_contact_resort_maps_url',
        'label' => 'Resort map url',
        'name' => 'contact_resort_maps_url',
        'type' => 'image',
        'parent' => 'contact_group',
        'return_format' => 'url',
    ));

    acf_add_local_field(array(
        'key' => 'field_contact_where_to_find_us',
        'label' => 'Where to find us',
        'name' => 'contact_where_to_find_us',
        'type' => 'text',
        'parent' => 'contact_group',
    ));


    acf_add_local_field(array(
        'key' => 'field_contact_show_resort_map',
        'label' => 'Show resort map',
        'name' => 'contact_show_resort_map',
        'type' => 'text',
        'parent' => 'contact_group',
    ));

    acf_add_local_field(array(
        'key' => 'field_contact_form',
        'label' => 'Context form',
        'name' => 'contact_form',
        'type' => 'text',
        'parent' => 'contact_group',
    ));

    acf_add_local_field(array(
        'key' => 'field_contact_fields',
        'label' => 'Contact fields',
        'name' => 'contact_fields',
        'type' => 'text',
        'parent' => 'contact_group',
    ));

    acf_add_local_field(array(
        'key' => 'field_contact_date_and_apartment',
        'label' => 'Date and apartment',
        'name' => 'contact_date_and_apartment',
        'type' => 'text',
        'parent' => 'contact_group',
    ));

    acf_add_local_field(array(
        'key' => 'field_contact_form_continue',
        'label' => 'Continue button',
        'name' => 'contact_form_continue',
        'type' => 'text',
        'parent' => 'contact_group',
    ));

    acf_add_local_field(array(
        'key' => 'field_contact_form_submit',
        'label' => 'Submit button',
        'name' => 'contact_form_submit',
        'type' => 'text',
        'parent' => 'contact_group',
    ));

    acf_add_local_field(array(
        'key' => 'field_contact_privacy_policy',
        'label' => 'Privacy policy',
        'name' => 'contact_privacy_policy',
        'type' => 'text',
        'parent' => 'contact_group',
    ));

    acf_add_local_field(array(
        'key' => 'field_contact_form_name',
        'label' => 'First and last name',
        'name' => 'contact_form_name',
        'type' => 'text',
        'parent' => 'contact_group',
    ));

    acf_add_local_field(array(
        'key' => 'field_contact_form_email',
        'label' => 'Email',
        'name' => 'contact_form_email',
        'type' => 'text',
        'parent' => 'contact_group',
    ));

    acf_add_local_field(array(
        'key' => 'field_contact_form_number',
        'label' => 'Phone number',
        'name' => 'contact_form_number',
        'type' => 'text',
        'parent' => 'contact_group',
    ));
    acf_add_local_field(array(
        'key' => 'field_contact_form_from_to',
        'label' => 'Date from to',
        'name' => 'contact_form_from_to',
        'type' => 'text',
        'parent' => 'contact_group',
    ));
    acf_add_local_field(array(
        'key' => 'field_contact_form_persons',
        'label' => 'Persons',
        'name' => 'contact_form_persons',
        'type' => 'text',
        'parent' => 'contact_group',
    ));

    acf_add_local_field(array(
        'key' => 'field_contact_villas',
        'label' => 'Villas',
        'name' => 'contact_villas',
        'type' => 'repeater',
        'layout' => 'table',
        'parent' => 'contact_group',
        'sub_fields' => array(
            array(
                'key' => 'field_contact_villa_page',
                'label' => 'Villa page',
                'name' => 'contact_villa_page',
                'type' => 'page_link'
            ),
            array(
                'key' => 'field_contact_villa',
                'label' => 'Villa',
                'name' => 'contact_villa',
                'type' => 'text',
            ),
        ),
    ));
endif;