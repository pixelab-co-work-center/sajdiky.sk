<?php
/**
 * Template Name: Villa Template
 */
?>
<?php get_header(); global $post; $is_en=false; if(get_locale() == 'en_US'){$is_en = "_en";} ?>
<div class="villa-info container-lg">
    <div class="villa-info__white-box"></div>
    <div class="row no-gutters align-items-start">
        <div class="col-md-5 villa-info__image">
            <?php
            foreach (get_field("villa_gallery") as $i => $image) {
                ?>
                <?php if ($i == 0): ?>
                    <a href="<?= $image["url"] ?>" data-lightbox="gallery" data-title="<?= $image["caption"] ?>"
                       id="gallery-first-image">
						<div class="gallery_image" style="background-image: url('<?= $image["url"] ?>')"></div>
                        <span class="open-gallery villa-info__image__gallery-button villa-info__image__gallery-button--number">
                        + <?= count(get_field("villa_gallery")) - 1 ?>
                    </span>
                        <span class="open-gallery villa-info__image__gallery-button villa-info__image__gallery-button--full">
                        <i class="fas fa-expand-alt fa-2x"></i>
                    </span>
                    </a>

                <?php else: ?>
                    <a href="<?= $image["url"] ?>" data-lightbox="gallery" data-title="<?= $image["caption"] ?>"
                       style="display: none;"></a>
                <?php endif; ?>
                <?php
            }
            ?>
        </div>
        <div class="col-md-7 villa-info__description">
            <div class="row  no-gutters">
                <div class="col-6 col-sm-6 col-md-6 holder--title">
                    <h1><?php the_field('villa_title'); ?></h1>
                </div>
                <div class="col-6 col-sm-6 col-md-6 title_line">
                    <hr>
                </div>
            </div>
            <div class="villa__text"><?php the_field('villa_description'); ?></div>

            <a href="<?php the_field('order_page'.($is_en ?: ""), 'option'); ?>"
               class="button inverted"><?php the_field('order_text'.($is_en ?: ""), 'option'); ?><i
                        class="fas fa-arrow-right"></i></a>
        </div>
    </div>
</div>

<div class="container-lg">
    <div class="row">
        <div class="col-12">
            <div class="equipment">
                <h2><?php the_field('field_villa_equipment_title'); ?></h2>
                <div class="row">
                    <?php
                    if (have_rows('villa_equipment_repeater')):
                        while (have_rows('villa_equipment_repeater')) : the_row(); ?>
                            <div class="col-md-3 col-sm-6">
                                <?php the_sub_field('villa_equipment_list'); ?>
                            </div>
                        <?php
                        endwhile;
                    endif;
                    ?>
                </div>
                <a href="<?php the_field('download_home_rules_file'); ?>" class="button inverted" target="_blank"><i
                            class="fas fa-download"></i><?php the_field('download_home_rules_text'.($is_en ?: ""), 'option'); ?></a>
            </div>
        </div>
        <div class="information_holder">
			<div class="col-md-4">
				<div class="important-info">
					<h2><?php the_field('important_info_title'.($is_en ?: ""), 'option'); ?></h2>
					<div>
				        <?php the_field('important_info_text'); ?>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pricelist">
					<h2><?php the_field('pricelist_title'.($is_en ?: ""), 'option'); ?></h2>
					<div>
				        <?php the_field('pricelist_text'); ?>
					</div>
					<a class="button inverted" href="<?php the_field('pricelist_page'); ?>"><?php the_field('request_offer'.($is_en ?: ""), 'option'); ?><i
								class="fas fa-arrow-right"></i></a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="location">
					<h2><?php the_field('locality'.($is_en ?: ""), 'option'); ?></h2>
					<div id="villa-map" class="map"></div>
					<script>
				        maps.push(function () {
					        var point = {
						        lat: <?=get_field('villa_lat') ? get_field('villa_lat') : 0; ?>,
						        lng: <?=get_field('villa_lng') ? get_field('villa_lng') : 0; ?>
					        };
					        var map = new google.maps.Map(document.getElementById('villa-map'), {
						        zoom: 4,
						        center: point
					        });
					        var marker = new google.maps.Marker({position: point, map: map});
				        });
					</script>
				</div>
			</div>
		</div>
    </div>
</div>
<div class="paging">
    <div class="container-lg">
        <div class="row">
            <div class="col-6 paging__prev">
                <a href="<?php the_field('prev_villa_page'); ?>"><i
                            class="fas fa-arrow-left"></i><span><?php the_field('prev_villa_title'); ?></span></a>
            </div>
            <div class="col-6 paging__next">
                <a href="<?php the_field('next_villa_page'); ?>"><span><?php the_field('next_villa_title'); ?></span><i
                            class="fas fa-arrow-right"></i></a>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>
