// mobile menu function

jQuery(document).ready(function ($) {

    var _button = document.getElementsByClassName('menu__icon');

    $(_button).on('click', function () {
        if ($('.navigation--mobile').css('left') === "0px") {
        	$('body').css({'overflow': 'auto'});
            $('.navigation--mobile').animate({
                left: '100%'
            }, {
                duration: 200, complete: function () {
                    $(this).css({display: 'none'});
                }
            });

            $('.collapse_bar--center-top').animate(
                {deg: 0},
                {
                    duration: 300,
                    step: function (now) {
                        $(this).css({transform: 'rotate(' + now + 'deg)'});
                    }, queue: false
                });
            $('.collapse_bar--center-bottom').animate(
                {deg: 0},
                {
                    duration: 300,
                    step: function (now) {
                        $(this).css({transform: 'rotate(' + now + 'deg)'});
                    }, queue: false
                });
            $('.collapse_bar--top').animate({
                opacity: 1
            }, 100);
            $('.collapse_bar--bottom').animate({
                opacity: 1
            }, 100);


        }
        else {
	        $('body').css({'overflow': 'hidden'});
            $('.navigation--mobile').css({display: 'flex'}).animate({
                left: 0
            }, {duration: 300});
            $('.collapse_bar--center-top').animate(
                {deg: 45},
                {
                    duration: 500,
                    step: function (now) {
                        $(this).css({transform: 'rotate(' + now + 'deg)'});
                    }, queue: false
                });
            $('.collapse_bar--top').animate({
                opacity: 0
            }, 100);
            $('.collapse_bar--bottom').animate({
                opacity: 0
            }, 100);
            $('.collapse_bar--center-bottom').animate(
                {deg: -45},
                {
                    duration: 500,
                    step: function (now) {
                        $(this).css({transform: 'rotate(' + now + 'deg)'});
                    }, queue: false
                });
        }
    });

    // language
	if(document.documentElement.lang.toLowerCase() === "en-us"){
		$('.active_lang').removeClass('active_lang');
		$('.lang-item-en').addClass('active_lang');
	}
	else if(document.documentElement.lang.toLowerCase() === "sk-sk"){
		$('.active_lang').removeClass('active_lang');
		$('.lang-item-sk').addClass('active_lang');
	}
});

