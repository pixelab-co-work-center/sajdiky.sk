</main>
<footer>
    <div class="container-lg">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-sm-6 footer-logo">
                        <?php
                        $custom_logo_id = get_theme_mod('custom_logo');
                        $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                        if (has_custom_logo()) {
                            echo '<img style="width: 190px;height: auto;" src="' . esc_url($logo[0]) . '" alt="' . get_bloginfo('name') . '">';
                        } else {
                            echo get_bloginfo('name');
                        }
                        ?>
                    </div>
                    <div class="col-sm-6 info text-sm-left">
                        <div class="row">
                            <div class="col-12 col-sm-2">
                                <i class="far fa-user"></i>
                            </div>
                            <div class="col-12 col-sm-10 info__text">
                                <a href="tel:<?php the_field('footer_phone_number'.($is_en ?: ""), 'option');?>"><?php the_field('footer_phone_number'.($is_en ?: ""), 'option'); ?></a>
								<a href="mailto:<?php the_field('footer_email'.($is_en ?: ""), 'option');?>"><?php the_field('footer_email'.($is_en ?: ""), 'option');?></a>
                                <span><?php the_field('footer_name'.($is_en ?: ""), 'option'); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-2">
                                <i class="fas fa-map-marker-alt"></i>
                            </div>
                            <div class="col-12 col-sm-10 info__text">
                                <span><?php the_field('footer_street'.($is_en ?: ""), 'option'); ?></span>
                                <span><?php the_field('footer_city'.($is_en ?: ""), 'option'); ?></span>
                            </div>
                        </div>
						<div class="row">
							<div class="col-lg-12 social">
								<a href="<?php the_field('footer_facebook'.($is_en ?: ""), 'option'); ?>">
									<i class="fab fa-facebook-square fa-2x"></i>
								</a>
								<a href="<?php the_field('footer_instagram'.($is_en ?: ""), 'option'); ?>">
									<i class="fab fa-instagram fa-2x"></i>
								</a>
							</div>
						</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="row  no-gutters">
                    <div class="col-lg-12">
                        <div id="map"></div>
                        <script>
                            maps.push(function () {
                                var point = {
                                    lat: <?=get_field('footer_lat'.($is_en ?: ""), 'option') ? get_field('footer_lat'.($is_en ?: ""), 'option') : 0; ?>,
                                    lng: <?=get_field('footer_lat'.($is_en ?: ""), 'option') ? get_field('footer_lng'.($is_en ?: ""), 'option') : 0; ?>
                                };
                                var map = new google.maps.Map(document.getElementById('map'), {zoom: 4, center: point});
                                var marker = new google.maps.Marker({position: point, map: map});
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright"></div>
</footer>
<script>
    function initMaps() {
        for (let i in maps) {
            maps[i]();
        }
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=<?php the_field('google_maps_api_key'.($is_en ?: ""), 'option'); ?>&callback=initMaps">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/dist/scripts/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>