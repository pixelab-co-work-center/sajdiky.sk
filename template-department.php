<?php
/**
 * Template Name: Resort Template
 */
?>
<?php get_header() ?>
    <div class="department-header">
        <div class="container">
            <div class="row">
                <div class="col-5 col-sm-4 col-md-2">
                    <h1><?php the_field('resort'); ?></h1>
                </div>
                <div class="col-7 col-sm-8 col-md-10">
                    <hr>
                </div>
            </div>
        </div>
    </div>
    <div class="departments">
        <div class="container">
            <div class="row">
                <?php
                $i = 0;
                if (have_rows('departments_repeater')):
                    while (have_rows('departments_repeater')) : the_row(); ?>
                        <div class="col-lg-4 col-md-6 col-sm-12 <?= $i == 2 ? "last" : "" ?>">
                            <div class="departments__department">
                                <h3>
                                    <?php the_sub_field('department_name'); ?>
                                </h3>
                                <?php if (get_sub_field('department_text')): ?>
                                    <p>
                                        <?php the_sub_field('department_text'); ?>
                                    </p>
                                <?php endif; ?>
                                <?php if (get_sub_field('department_email')): ?>
                                    <div>
                                        <i class="far fa-envelope"></i><a class="link--website" href="mailto:<?php the_sub_field('department_email'); ?>"><?php the_sub_field('department_email'); ?></a>
                                    </div>
                                <?php endif; ?>

                                <?php if (get_sub_field('department_phone_number')): ?>
                                    <div>
                                        <i class="fas fa-phone-alt"></i><a class="link--website" href="tel:<?php the_sub_field('department_phone_number'); ?>"><?php the_sub_field('department_phone_number'); ?></a>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_sub_field('department_time')): ?>
                                    <div>
                                        <i class="far fa-clock"></i><?php the_sub_field('department_time'); ?>
                                    </div>
                                <?php endif; ?>
	                            <?php if (get_sub_field('department_website')): ?>
									<div>
										<?php $website = get_sub_field('department_website');?>
										<i class="fas fa-mouse-pointer"></i><a class="link--website" href="<?= $website['url'];?>"><?= $website['title'];?></a>
									</div>
	                            <?php endif; ?>
                                <?php if ($i == 1): ?>
                                    <div class="departments__department__downloads">
                                        <a href="<?php the_field('lunch_menu_file'); ?>" class="button inverted"><i
                                                    class="fas fa-download"></i><?php the_field('lunch_menu_text'); ?>
                                        </a>
                                        <span class="departments-downloads__space"></span>
                                        <a href="<?php the_field('drink_menu_file'); ?>" class="button inverted"><i
                                                    class="fas fa-download"></i><?php the_field('drink_menu_text'); ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php

                        $i++;
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
<?php get_footer() ?>