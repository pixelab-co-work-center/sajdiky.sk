<?php
/*
Plugin Name: Custom contact form
Description: Simple form plugin
Version: 1.2.1
Author: Marián Ligocký - Pixelab
*/
include_once 'pixelab-contact-form-admin.php';

class PixelabContactForm
{
    protected $options;

    public function __construct()
    {
        $this->options = get_option('px_form_options');
        add_action('wp_mail_failed', array($this, 'action_wp_mail_failed'));
        // Add Javascript and CSS for front-end display
        add_action('wp_enqueue_scripts', array($this, 'enqueue'));
        add_action('wp_ajax_px_deliver_mail', array($this, 'deliver_email'));
        add_action('wp_ajax_nopriv_px_deliver_mail', array($this, 'deliver_email'));
    }

    public function install()
    {
        // Used for google recaptcha v3 key and site
        add_option('px_form_options', '');
    }

    public function uninstall()
    {
        delete_option('px_recaptcha_key');
        delete_option('px_recaptcha_secret');
    }

    public function echo_message($type = 'success', $message = '', $internal_message = '')
    {
        $message = ['type' => $type, 'message' => $message, 'internal_message' => $internal_message];
        echo json_encode($message);
    }

    // scripts
    public function enqueue()
    {
        wp_enqueue_script('recaptcha', esc_url('https://www.google.com/recaptcha/api.js?render=' . $this->options['px_recaptcha_key']), array('jquery'), '1.0', true);
        wp_enqueue_script('px-form', plugins_url('js/form.js', __FILE__), array('jquery'), '1.2', true);

        $dataToBePassed = array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'recaptcha_key' => $this->options['px_recaptcha_key']
        );
        wp_localize_script('px-form', 'php_vars', $dataToBePassed);
    }

    public function deliver_email()
    {

        header('Content-type: application/json');
        // if the submit button is clicked, send the email
        if (isset($_POST['px_form_data'])) {

            if (!isset($_POST['px_form_data']['privacy-policy'])) {
                $this->echo_message('error', 'Musíte súhlasiť so spracovaním osobných údajov.');
                wp_die();
            }
            // sanitize form values
            $name = sanitize_text_field($_POST['px_form_data']['name']);
            $email = sanitize_email($_POST['px_form_data']['email']);
            $phone_number = $_POST['px_form_data']['phone-number'];
			
            $from_to = $_POST['px_form_data']['from_to'];
            $persons = intval($_POST['px_form_data']['persons']);
            $villa = $_POST['px_form_data']['villa'];
			$message = esc_textarea($_POST['px_form_data']['message']);

            $captcha = sanitize_text_field($_POST['px_form_data']['g-recaptcha-response']);
            $recaptcha_secret = $this->options['px_recaptcha_secret'];
			
            $subject = "Šajdíky | Nová správa od $name";
            $email_message = "";
            $email_message .= "Meno: $name\n";
            $email_message .= "Email: $email\n";
            $email_message .= "Telefónne číslo: $phone_number\n";
            $email_message .= "---------------\n";
            $email_message .= "Dátum: $from_to\n";
            $email_message .= "Počet osôb: $persons\n";
            $email_message .= "Vila: $villa\n";
            $email_message .= "Správa: $message\n";

            // get the blog administrator's email address
            $to = $this->options['px_deliver_email'];

            $headers = "From: $name <$email>" . "\r\n" . "Reply-to: $email";
            if ($this->check_captcha($recaptcha_secret, $captcha)) {
                // If email has been process for sending, display a success message
                if (wp_mail($to, $subject, $email_message, $headers)) {
                    $this->echo_message('success', 'Email bol odoslaný.');
                    wp_die();
                } else {
                    $this->echo_message('error', 'Správa nebola odoslaná. Prosím pošlite nám súkromnú správu.', 'Captcha is ok, but did not send.');
                    wp_die();
                }
            } else {
                $this->echo_message('error', 'Máme problém s overením, že nie ste robot. Prosím pošlite nám súkromnú správu.', 'Captcha is not ok.');
                wp_die();
            }

        } else {
            $this->echo_message('error', 'Správa nebola odoslaná. Prosím pošlite nám súkromnú správu.', 'Did not receive any form data.');
            wp_die();
        }
        $this->echo_message('error', 'Správa nebola odoslaná. Prosím pošlite nám súkromnú správu.', 'Unknown error occured.');
        wp_die();
    }

    private function check_captcha($recaptcha_secret, $captcha)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = array('secret' => $recaptcha_secret, 'response' => $captcha);

        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context = stream_context_create($options);
        $response = file_get_contents($url, false, $context);
        $responseKeys = json_decode($response, true);
        header('Content-type: application/json');
        if ($responseKeys["success"]) {
            return true;
        } else {
            return false;
        }
    }

    function set_mail_content_type()
    {
        return "text/html";
    }

    function action_wp_mail_failed($wp_error)
    {
        return error_log(print_r($wp_error, true));
    }
}


global $pxForm;
global $pxFormAdmin;
if (class_exists('PixelabContactForm')) {
    if (is_admin() && !wp_doing_ajax()) {
        $pxFormAdmin = new PixelabContactFormAdmin();
    } else {
        $pxForm = new PixelabContactForm();
    }

}
register_activation_hook(__FILE__, array($pxFormAdmin, 'activate'));
register_deactivation_hook(__FILE__, array($pxFormAdmin, 'deactivate'));