<?php

class PixelabContactFormAdmin extends PixelabContactForm
{

    /**
     * PixelabContactFormAdmin constructor.
     */
    public function __construct()
    {
        parent::__construct();
        add_action('admin_menu', array($this, 'add_plugin_page_to_menu'));
        add_action('admin_init', array($this, 'admin_init'));
    }

    public function admin_init()
    {
        // Register settings (inputs) for options page
        $this->register_settings();
    }

    public function add_plugin_page_to_menu()
    {
        // This page will be under "Settings"
        add_options_page('Kontaktný formulár', 'Kontaktný formulár', 'manage_options', 'pixelab-contact-form', array($this, 'options_page'));
    }

    // register settings
    public function register_settings()
    {
        register_setting('px_form', 'px_form_options', array($this, 'sanitize'));
        add_settings_section(
            'px_recaptcha',
            'Nastavenia recaptche',
            array($this, 'print_section_info'),
            'px_form-admin-settings'
        );
        add_settings_section(
            'px_email',
            'Nastavenia emailu',
            null,
            'px_form-admin-settings'
        );
        add_settings_field(
            'px_recaptcha_key', // ID
            'Recaptcha kľúč', // Title
            array($this, 'display_input'), // Callback with parameter from last argument
            'px_form-admin-settings', // Page
            'px_recaptcha', // Section,
            array('field_name' => 'px_recaptcha_key')
        );

        add_settings_field(
            'px_recaptcha_secret', // ID
            'Recaptcha tajný kľúč', // Title
            array($this, 'display_input'), // Callback with parameter from last argument
            'px_form-admin-settings', // Page
            'px_recaptcha', // Section,
            array('field_name' => 'px_recaptcha_secret')
        );

        add_settings_field(
            'px_deliver_email', // ID
            'Emailová adresa', // Title
            array($this, 'display_input'), // Callback with parameter from last argument
            'px_form-admin-settings', // Page
            'px_email', // Section,
            array('field_name' => 'px_deliver_email')
        );
    }

    public function display_input($args)
    {
        printf(
            '<input type="text" id="' . $args['field_name'] . '" name="px_form_options[' . $args['field_name'] . ']" value="%s" class="regular-text" />',
            isset($this->options[$args['field_name']]) ? esc_attr($this->options[$args['field_name']]) : ''
        );
    }

    public function options_page()
    {
        ?>
        <div class="wrap">
            <h1>Nastavenie formuláru</h1>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('px_form');
                do_settings_sections('px_form-admin-settings');
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     * @return array
     */
    public function sanitize($input)
    {
        $new_input = array();
        if (isset($input)) {
            foreach ($input as $item => $value) {
                $new_input[$item] = sanitize_text_field($value);
            }
        }

        return $new_input;
    }

    public function print_section_info()
    {
        print 'Nastavenia získate na <a href="https://www.google.com/recaptcha/intro/v3.html">Google Recaptcha v3</a>';
    }
}