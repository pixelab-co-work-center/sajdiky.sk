var Form = function () {

}

Form.prototype.init = function () {
    var _this = this;
    jQuery('.form').submit(function (e) {
        e.preventDefault();
        console.log("clicked");
        _this.submitForm(jQuery(this));
    });
}

Form.prototype.submitForm = function (form) {
    var ajaxurl = php_vars.ajaxurl;

    grecaptcha.ready(function () {
        grecaptcha.execute(php_vars.recaptcha_key, {action: 'px_form_submit'}).then(function (token) {
            form.prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');
            var formData = form.serializeArray();
            var data = {};
            jQuery(formData).each(function (index, obj) {
                data[obj.name] = obj.value;
            });

            jQuery.post(ajaxurl, {
                action: "px_deliver_mail",
                px_form_data: data
            }, function (data) {
                form.find('.messages-last').empty();
                if (data.type == 'success') {
                    form.find('.messages-last').append('<div class="message message--success">' + data.message + '</div>');
                } else {
                    form.find('.messages-last').append('<div class="message message--fail">' + data.message + '</div>');
                    console.log("Internal form error:");
                    console.log(data.internal_message);
                }
            });
        });
    });

}

jQuery(function () {
    if (jQuery('.form').length > 0) {
        var form = new Form();
        form.init();
    }
});