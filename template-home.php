<?php /**
 * Template Name: Home Template
 */
?>
<?php get_header() ?>
    <div class="container-lg">
        <div class="row home-villas">
            <?php for ($i = 1; $i <= 3; $i++): ?>
                <div class="col-md-4 col-sm-12">
                    <div class="home-villas__villa">
                        <a href="<?php the_field('villa_' . $i . '_page'); ?>">
                            <h2><?php the_field('villa_' . $i . '_title'); ?></h2></a>
                        <p><?php the_field('villa_' . $i . '_description'); ?></p>
                        <a href="<?php the_field('villa_' . $i . '_page'); ?>"
                           class="bounce-link link"><?php the_field('read_more'); ?><i
                                    class="fas fa-arrow-right"></i></a>
                        <a href="<?php the_field('villa_' . $i . '_page'); ?>">
                            <div class="home_villa__image"
                                 style="background-image: url('<?php the_field('villa_' . $i . '_image'); ?>')"></div>
                        </a>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
    </div>
    <div class="home-villas__white-box"></div>
    <div class="guaranteed-rest">
        <div class="container-lg">
            <div class="row">
                <div class="col-md-4">
                    <h1><?php the_field('guaranteed_rest_title'); ?></h1>
                </div>
                <div class="col-md-8">
                    <hr>
                </div>
            </div>
            <?php the_field('guaranteed_rest_text'); ?>
            <a href="<?php the_field('field_order_now_page'); ?>"
               class="button bounce-link"><?php the_field('order_now_text'); ?><i
                        class="fas fa-arrow-right"></i></a>
        </div>
    </div>
    <div class="next-project container-lg d-flex">
        <div class="col-md-6">
            <h1><?php the_field('field_next_project_text'); ?></h1>
        </div>
        <div class="col-md-6 next-project__link">
            <a href="<?php the_field('field_golf_resort_page'); ?>" class="button inverted bounce-link">
                <img src="<?php the_field('golf_resort_image'); ?>">
                <?php the_field('golf_resort_text'); ?><i class="fas fa-arrow-right"></i>
            </a>
        </div>
    </div>
<?php get_footer() ?>