'use strict';

const sass = require('gulp-sass');
const {src, dest, watch} = require('gulp');
const browserSync = require("browser-sync").create();
const include = require('gulp-include')

function compileSass(done) {
    src('./assets/styles/app.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(dest('./dist/styles/'));

    src('./assets/images/*')
        .pipe(dest('./dist/images/'));

    done();
}

function compileJs(done) {
    src('./assets/scripts/main.js')
        .pipe(include())
        .on('error', console.log)
        .pipe(dest('./dist/scripts/'));

    done();
}

function watchAll() {
    browserSync.init({
        proxy: "http://localhost/pixelab/sajdiky.sk/"
    });
    watch('./assets/images/*', src('./assets/images/*')
        .pipe(dest('./dist/images/'))).on('change', browserSync.reload);

    watch('./assets/styles/**', compileSass).on('change', browserSync.reload);

    watch('./assets/scripts/**', compileJs).on('change', browserSync.reload);

    watch('./*.php').on('change', browserSync.reload);
}

exports.watch = watchAll;
exports.sass = compileSass;
exports.js = compileJs;