<?php global $post; $is_en=false; if(get_locale() == 'en_US'){$is_en = "_en";} ?>
<!DOCTYPE html>
<html <?php language_attributes() ?>>
<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php bloginfo('title') ?></title>
    <link href="https://fonts.googleapis.com/css2?family=Tinos:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <?php wp_head(); ?>
</head>

<body <?php body_class() ?>>
<script>
    maps = [];
</script>
<header class="site-header">
    <div class="container-lg">
        <nav class="navbar navbar-default navbar-expand-md">
            <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                <?php
                $custom_logo_id = get_theme_mod('custom_logo');
                $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                if (has_custom_logo()) {
                    echo '<img src="' . esc_url($logo[0]) . '" alt="' . get_bloginfo('name') . '">';
                } else {
                    echo get_bloginfo('name');
                }
                ?>
            </a>

	        <?php wp_nav_menu( array(
		        'theme_location'  => 'primary',
		        'menu_class'      => 'navbar-nav',
		        'container'       => 'div',
		        'container_class' => 'navbar-desktop',
		        'items_wrap' => '<ul class="%2$s">%3$s</ul>',
	        ))?>

            <span class="navbar-text ml-auto">
                <a class="tel" href="tel:<?php the_field('phone_number'.($is_en ?: ""), 'option'); ?>"><i class="fas fa-phone-alt"></i><?php the_field('phone_number'.($is_en ?: ""), 'option'); ?></a>
				<?php wp_nav_menu( array(
		            'theme_location'  => 'language',
		            'menu_class'      => 'languages',
		            'container'       => 'div',
		            'container_class' => 'language_switcher',
		            'items_wrap' => '<ul class="%2$s">%3$s</ul>',
	            ))?>
            </span>


            <div id="navbar-mobile">
                <div class="menu__icon__holder">
                    <div class="menu__icon">
                        <span class="menu__icon--span collapse_bar--top" style="opacity: 1;"></span>
                        <span class="menu__icon--span collapse_bar--center-top" style="transform: rotate(0deg);"></span>
                        <span class="menu__icon--span collapse_bar--center-bottom"
                              style="transform: rotate(0deg);"></span>
                        <span class="menu__icon--span collapse_bar--bottom" style="opacity: 1;"></span>
                    </div>
                </div>
                <nav class="navigation--mobile" style="left: 100%; display: none;">
                    <ul>
                        <?php
                        $menu = wp_get_nav_menu_items('primary');
                        if (!empty($menu)) {
                            foreach ($menu as $item) {
                                $id = get_the_id();
                                if (is_home()) {
                                    $id = intval(get_option('page_for_posts'));
                                }
                                ?>
                                <li class="<?php if ($id === intval($item->object_id)) {
                                    echo ' active';
                                } ?>">
                                    <a href="<?php echo $item->url; ?>" class="nav-link"><?php echo $item->title; ?></a>
                                </li>
                                <?php
                            }
                        }
                        ?>
						<li>
							<a class="tel" href="tel:<?php the_field('phone_number'.($is_en ?: ""), 'option'); ?>"><i class="fas fa-phone-alt"></i><?php the_field('phone_number'.($is_en ?: ""), 'option'); ?></a>
						</li>
						<?php wp_nav_menu( array(
		                    'theme_location'  => 'language',
		                    'menu_class'      => 'languages',
		                    'container'       => 'div',
		                    'container_class' => 'language_switcher',
		                    'items_wrap' => '<ul class="%2$s">%3$s</ul>',
	                    ))?>
                    </ul>
                </nav>
            </div>
        </nav>
    </div>
</header>
<main>
